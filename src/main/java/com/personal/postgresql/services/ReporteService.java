package com.personal.postgresql.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import com.personal.postgresql.constants.ConstantesRespuestas;
import com.personal.postgresql.dto.ReporteDTO;
import com.personal.postgresql.entities.Reporte;
import com.personal.postgresql.mappers.ReporteMapper;
import com.personal.postgresql.repositories.ReporteDao;
import com.personal.postgresql.responses.RespuestaGenerica;



@Service
public class ReporteService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final ReporteMapper reporteMapper = new ReporteMapper();
	
	@Autowired
	private ReporteDao reporteDao;
	
	public @ResponseBody ResponseEntity<RespuestaGenerica> guardarReporte(ReporteDTO reporteDtoAGuardar){
		try {			
			RespuestaGenerica respuestaGenerica;
			Reporte reporteAGuardar = this.reporteMapper.obtenerReporte(reporteDtoAGuardar);
			this.reporteDao.save(reporteAGuardar);
			respuestaGenerica = new RespuestaGenerica(ConstantesRespuestas.EXITO, "");
			return new ResponseEntity<RespuestaGenerica>(respuestaGenerica, HttpStatus.OK);
		}catch (Exception e) {
			logger.error("Error se presentan problemas al guardar el reporte de aviones");
			e.printStackTrace();
			return new ResponseEntity<RespuestaGenerica>(new RespuestaGenerica(ConstantesRespuestas.ERRROR, ""),
					HttpStatus.BAD_REQUEST);
		}
	}
	
	public @ResponseBody ResponseEntity<RespuestaGenerica> obtenerReportes(int pageRequest,
			int idTipoRegistro, boolean buscar) {
		try {
			RespuestaGenerica<ReporteDTO> respuestaGenerica;
			Pageable pageableConfig = PageRequest.of(pageRequest, 5);
			Page<Reporte> listaReporte;
			if (buscar && idTipoRegistro > 0) {
				listaReporte = reporteDao.obtenerReporteFiltrado(Long.valueOf(idTipoRegistro),pageableConfig);
			} else {
				listaReporte = reporteDao.findAllPaged(pageableConfig);

			}
			List<ReporteDTO> result = new ArrayList<>();
			listaReporte.forEach(reporteACastear -> {
				result.add(reporteMapper.obtenerReporteDTO(reporteACastear));
			});
			respuestaGenerica = new RespuestaGenerica<>(result, "EXITO", listaReporte.getTotalElements() + "");
			return new ResponseEntity<RespuestaGenerica>(respuestaGenerica, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error se presentan problemas al cargar los reportes del avión ");
			return new ResponseEntity<RespuestaGenerica>(
					new RespuestaGenerica<>("ERROR", ""), HttpStatus.BAD_REQUEST);
		}
	}
	
	
}
