package com.personal.postgresql.mappers;

import com.personal.postgresql.dto.MaestroValorDTO;
import com.personal.postgresql.dto.ReporteDTO;
import com.personal.postgresql.entities.MaestroValor;
import com.personal.postgresql.entities.Reporte;

import lombok.Data;

@Data
public class ReporteMapper {
	
	private final MaestroValorMapper maestroValorMapper = new MaestroValorMapper();

	public ReporteDTO obtenerReporteDTO(Reporte reporteACastear) {

		MaestroValor maestroValorACastearPorUnDTO = reporteACastear.getTipoMovimientoAvion();
		
		MaestroValorDTO maestroARetornar = this.maestroValorMapper
				.obtenerMaestroValorDTO(maestroValorACastearPorUnDTO);

		ReporteDTO reporteARetornar = new ReporteDTO();
		reporteARetornar.setId(reporteACastear.getId());
		reporteARetornar.setDistancia(reporteACastear.getDistancia());
		reporteARetornar.setAltura(reporteACastear.getAltura());
		reporteARetornar.setFechaRegistro(reporteACastear.getFechaRegistro());
		reporteARetornar.setTipoMovimientoAvion(maestroARetornar);
		return reporteARetornar;
		
	}
	
	public Reporte obtenerReporte(ReporteDTO reporteDTOACastear) {

		MaestroValorDTO maestroValorACastearPorUnaEntidad = reporteDTOACastear.getTipoMovimientoAvion();
		
		MaestroValor maestroARetornar = this.maestroValorMapper
				.obtenerMaestroValor(maestroValorACastearPorUnaEntidad);

		Reporte reporteARetornar = new Reporte();
		reporteARetornar.setId(reporteDTOACastear.getId());
		reporteARetornar.setAltura(reporteDTOACastear.getAltura());
		reporteARetornar.setDistancia(reporteDTOACastear.getDistancia());
		reporteARetornar.setFechaRegistro(reporteDTOACastear.getFechaRegistro());
		reporteARetornar.setTipoMovimientoAvion(maestroARetornar);
		return reporteARetornar;
		
	}

}
