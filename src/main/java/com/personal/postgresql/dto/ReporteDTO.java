package com.personal.postgresql.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class ReporteDTO implements Serializable{
	private long id;
	private int distancia;
	private int altura;
	private Date fechaRegistro;
	private MaestroValorDTO tipoMovimientoAvion;
	
}
