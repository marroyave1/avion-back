package com.personal.postgresql.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.personal.postgresql.constants.ConstantesApis;
import com.personal.postgresql.dto.ReporteDTO;
import com.personal.postgresql.responses.RespuestaGenerica;
import com.personal.postgresql.services.ReporteService;

@Controller
@RequestMapping(value = ConstantesApis.REPORTE_API)
public class ReporteController {
	
	@Autowired
	private ReporteService reporteService;
	
	@PostMapping(value = ConstantesApis.REPORTE_GUARDAR)
	public @ResponseBody ResponseEntity<RespuestaGenerica> guardarReporte(
			@RequestBody ReporteDTO reporteDtoAGuardar){
		return this.reporteService.guardarReporte(reporteDtoAGuardar);
	}
	
	@GetMapping(value = ConstantesApis.REPORTE_LISTAR_REPORTES, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<RespuestaGenerica> obtenerReportes(
			@RequestParam int pageRequest, @RequestParam(required = false,defaultValue = "0") int idTipoRegistro,
			@RequestParam(required = false,defaultValue = "false") boolean buscar) {
		return reporteService.obtenerReportes(pageRequest,idTipoRegistro,buscar);
	}

}
