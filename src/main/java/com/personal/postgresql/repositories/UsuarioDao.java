package com.personal.postgresql.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.personal.postgresql.entities.Usuario;

@Repository
public interface UsuarioDao extends CrudRepository<Usuario, Long>{
 
	
	@Query(value = "select * from USUARIO where UPPER(nombres) like UPPER((CONCAT('%', :userName, '%'))) "
			+ "and documento like (CONCAT('%', :userDocument, '%'))", nativeQuery = true)
	Page<Usuario> buscarConFiltro(@Param("userName") String userName, @Param("userDocument") String userDocument, Pageable pageableConfig);
	
	@Query(value = "select * from USUARIO", nativeQuery = true)
	Page<Usuario> findAllPaged(Pageable pageableConfigs);
	
}
