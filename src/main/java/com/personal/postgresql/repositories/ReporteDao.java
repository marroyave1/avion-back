package com.personal.postgresql.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.personal.postgresql.entities.Reporte;

@Repository
public interface ReporteDao extends CrudRepository<Reporte, Long>{
	
	@Query("Select r from Reporte r")
	Page<Reporte> findAllPaged(Pageable pageableConfigs);

	@Query("Select r from Reporte r where r.tipoMovimientoAvion.id = ?1")
	Page<Reporte> obtenerReporteFiltrado(Long idTipoRegsitro, Pageable pageableConfig);

}
